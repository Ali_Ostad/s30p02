<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{
    public $resizeNameImage;

    // TODO comment
    public function download(string $name, int $height = null, int $width = null)
    {
        $checkType = $this->checkTypeFile($name);
        // TODO use {} for all if
        if (!$checkType) return "Wrong Type File! Only: png , jpeg , jpg";

        $checkExist = $this->searchFile($name,"image");
        if (!$checkExist) return "File not Exist";

        if ($height !== null && $width !== null) {
            if (!$this->checkPixel($height)) return "Height and Width must be between 50 to 150";
            if (!$this->checkPixel($width)) return "Height and Width must be between 50 to 150";

            $this->resizeImage($name,$height,$width);
            return Storage::disk("resized-images")->download($this->resizeNameImage);
        }

        return Storage::disk("image")->download($name);
    }


    public function checkTypeFile($name)
    {
        // TODO use regex for this kind of check
        $png = strstr($name, ".png");
        $jpg = strstr($name, ".jpg");
        $jpeg = strstr($name, ".jpeg");

        return ($png || $jpg || $jpeg) ? true : false;

    }


    public function searchFile($name,$directory)
    {
        $files = Storage::disk($directory)->files();
        // TODO in_array method return boolean itself ( INSTALL Sonarlint plugin in phpstorm)
        return (in_array($name, $files)) ? true : false;
    }


    public function checkPixel($param)
    {
        return (50 <= $param && $param <= 150) ? true : false;
    }


    public function resizeImage($name,$height,$width)
    {
        // TODO check existing of picture should implement outside of this function
        if(!$this->checkExistResizeImage($name,$height,$width))
        {

            Image::make(storage_path("app/images/".$name))->resize($width,$height)->save(storage_path('app/resized-images/').$this->resizeNameImage);
            return;
        }
        return;

    }

    public function checkExistResizeImage($name,$height,$width)
    {
        $this->setNameForResizeImage($name,$height,$width);

        $checkExist = $this->searchFile($this->resizeNameImage,"resized-images");
        return ($checkExist) ? true : false;
    }

    public function setNameForResizeImage($name,$height,$width)
    {
        // TODO check out my code 
        $types = array(".png",".jpg",".jpeg");
        foreach ($types as $value)
        {
            $name = str_replace($value,"",$name);
        }
        // TODO it is better to not use property for this kind of variable (resizeNameImage) 
        $this->resizeNameImage = $name."-".$height."-".$width.".png";
    }
}
